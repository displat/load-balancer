import Config

config :displat_lb,
  token: System.get_env("DISPLAT_LB_DISCORD_TOKEN"),
  shards: 1,
  intents: 0
