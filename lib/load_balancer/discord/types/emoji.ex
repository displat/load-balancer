defmodule LoadBalancer.Discord.Types.Emoji do
  @moduledoc "See [the documentaiton](https://discord.com/developers/docs/resources/emoji#emoji-object)"

  alias LoadBalancer.Discord.Types.{Snowflake, User}

  defstruct [
    :id,
    :name,
    :user,
    :require_colons,
    :managed,
    :animated,
    :roles,
    :available
  ]

  @typedoc "Id of the emoji"
  @type id :: Snowflake.t() | nil

  @typedoc "Name of the emoji"
  @type name :: String.t()

  @typedoc "Roles this emoji is whitelisted to"
  @type roles :: [Snowflake.t()] | nil

  @typedoc "User that created this emoji"
  @type user :: User.t() | nil

  @typedoc "Whether this emoji must be wrapped in colons"
  @type require_colons :: boolean | nil

  @typedoc "Whether this emoji is managed"
  @type managed :: boolean | nil

  @typedoc "Whether this emoji is animated"
  @type animated :: boolean | nil

  @typedoc "whether this emoji can be used, may be false due to loss of Server Boosts"
  @type available :: boolean | nil

  @type t :: %__MODULE__{
          id: id,
          name: name,
          roles: roles,
          user: user,
          require_colons: require_colons,
          managed: managed,
          animated: animated,
          available: available
        }
end
