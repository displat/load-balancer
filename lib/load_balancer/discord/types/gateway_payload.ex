defmodule LoadBalancer.Discord.Types.GatewayPayload do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/gateway#payloads-gateway-payload-structure)"

  defstruct [
    :op,
    :d,
    :s,
    :t
  ]

  @typedoc "opcode for the payload"
  @type opcode :: integer()

  @typedoc "event data"
  @type event :: map() | nil

  @typedoc "sequence number, used for resuming sessions and heartbeats"
  @type sequence_number :: integer() | nil

  @typedoc "the event name for this payload"
  @type event_name :: String.t() | nil

  @type t :: %__MODULE__{
          op: opcode,
          d: event,
          s: sequence_number,
          t: event_name
        }
end
