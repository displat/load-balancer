defmodule LoadBalancer.Discord.Types.SessionStartLimit do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/gateway#session-start-limit-object)"

  defstruct [
    :total,
    :remaining,
    :reset_after,
    :max_concurrency
  ]

  @typedoc "The total number of session starts the current user is allowed"
  @type total :: integer()

  @typedoc "The remaining number of session starts the current user is allowed"
  @type remaining :: integer()

  @typedoc "The number of milliseconds after which the limit resets"
  @type reset_after :: integer()

  @typedoc "The number of identify requests allowed per 5 seconds"
  @type max_concurrency :: integer()

  @type t :: %__MODULE__{
          total: total,
          remaining: remaining,
          reset_after: reset_after,
          max_concurrency: max_concurrency
        }
end
