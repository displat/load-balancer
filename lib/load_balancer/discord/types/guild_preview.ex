defmodule LoadBalancer.Discord.Types.GuildPreview do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/guild#guild-preview-object-guild-preview-structure)"

  alias LoadBalancer.Discord.Types.{Emoji, Guild}

  defstruct [
    :id,
    :name,
    :icon,
    :splash,
    :discovery_splash,
    :emojis,
    :features,
    :approximate_member_count,
    :approximate_presence_count,
    :description
  ]

  @typedoc "guild id"
  @type id :: Guild.id()

  @typedoc "guild name (2-100 characters)"
  @type name :: String.t()

  @typedoc "icon hash"
  @type icon :: String.t() | nil

  @typedoc "splash hash"
  @type splash :: String.t() | nil

  @typedoc "discovery splash hash"
  @type discovery_splash :: String.t() | nil

  @typedoc "custom guild emojis"
  @type emojis :: [Emoji.t()]

  @typedoc "enabled guild features"
  @type features :: [Guild.features()]

  @typedoc "approximate number of members in this guild"
  @type approximate_member_count :: integer()

  @typedoc "approximate number of online members in this guild"
  @type approximate_presence_count :: integer()

  @typedoc "the description for the guild"
  @type description :: String.t() | nil

  @type t :: %__MODULE__{
          id: id,
          name: name,
          icon: icon,
          splash: splash,
          discovery_splash: discovery_splash,
          emojis: emojis,
          features: features,
          approximate_member_count: approximate_member_count,
          approximate_presence_count: approximate_presence_count,
          description: description
        }
end
