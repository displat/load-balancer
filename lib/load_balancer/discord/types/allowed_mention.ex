defmodule LoadBalancer.Discord.Types.AllowedMention do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/channel#allowed-mentions-object)"

  alias LoadBalancer.Discord.Types.Guild.Role
  alias LoadBalancer.Discord.Types.User

  defstruct [
    :parse,
    :roles,
    :users,
    :replied_user
  ]

  @typedoc "An array of allowed mention types to parse from the content."
  @type parse :: [allowed_mention_type]

  @typedoc ~S{One of "roles", "users", or "everyone"}
  @type allowed_mention_type :: String.t()

  @typedoc "Array of role_ids to mention (Max size of 100)"
  @type roles :: [Role.id()]

  @typedoc "Array of user_ids to mention (Max size of 100)"
  @type users :: [User.id()]

  @typedoc "For replies, whether to mention the author of the message being replied to (default false)"
  @type replied_user :: boolean()

  @type t :: %__MODULE__{
          parse: parse,
          roles: roles,
          users: users,
          replied_user: replied_user
        }
end
