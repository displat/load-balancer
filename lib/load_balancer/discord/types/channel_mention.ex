defmodule LoadBalancer.Discord.Types.ChannelMention do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/channel#channel-mention-object)"

  alias LoadBalancer.Discord.Types.{Channel, Guild}

  defstruct [
    :id,
    :guild_id,
    :type,
    :name
  ]

  @typedoc "id of the channel"
  @type id :: Channel.id()

  @typedoc "id of the guild containing the channel"
  @type guild_id :: Guild.id()

  @typedoc "the type of channel"
  @type type :: Channel.type()

  @typedoc "the name of the channel"
  @type name :: Channel.name()

  @type t :: %__MODULE__{
          id: id,
          guild_id: guild_id,
          type: type,
          name: name
        }
end
