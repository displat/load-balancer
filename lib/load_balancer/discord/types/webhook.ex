defmodule LoadBalancer.Discord.Types.Webhook do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/webhook#webhook-object)"

  alias LoadBalancer.Discord.Types.{Channel, Guild, Snowflake, User}

  defstruct [
    :id,
    :type,
    :guild_id,
    :channel_id,
    :user,
    :name,
    :avatar,
    :token,
    :application_id
  ]

  @typedoc "Id of the webhook"
  @type id :: String.t()

  @typedoc "The type of the webhook"
  @type type :: integer

  @typedoc "Guild the webhook is for"
  @type guild_id :: Guild.t()

  @typedoc "Channel the webhook is for"
  @type channel_id :: Channel.t()

  @typedoc "User who created the webhook"
  @type user :: User.t()

  @typedoc "Default name of the webhook"
  @type name :: integer

  @typedoc "Default avatar of the webhook"
  @type avatar :: integer

  @typedoc "Secure token of the webhook"
  @type token :: integer

  @typedoc "The bot/OAuth2 application that created this webhook"
  @type application_id :: Snowflake.t() | nil

  @type t :: %__MODULE__{
          id: id,
          type: type,
          guild_id: guild_id,
          channel_id: channel_id,
          user: user,
          name: name,
          avatar: avatar,
          token: token,
          application_id: application_id
        }
end
