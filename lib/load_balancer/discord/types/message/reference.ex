defmodule LoadBalancer.Discord.Types.Message.Reference do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/channel#message-object-message-reference-structure)"

  alias LoadBalancer.Discord.Types.{Channel, Guild, Message}

  defstruct [
    :message_id,
    :channel_id,
    :guild_id
  ]

  @typedoc "id of the originating message"
  @type message_id :: Message.id() | nil

  @typedoc "id of the originating message's channel"
  @type channel_id :: Channel.id() | nil

  @typedoc "id of the originating message's guild"
  @type guild_id :: Guild.id() | nil

  @type t :: %__MODULE__{
          message_id: message_id,
          channel_id: channel_id,
          guild_id: guild_id
        }
end
