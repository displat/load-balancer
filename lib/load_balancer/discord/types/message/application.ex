defmodule LoadBalancer.Discord.Types.Message.Application do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/channel#message-object-message-application-structure)"

  alias LoadBalancer.Discord.Types.Snowflake

  defstruct [
    :id,
    :cover_image,
    :description,
    :icon,
    :name
  ]

  @typedoc "Id of the application"
  @type id :: Snowflake.t()

  @typedoc "Id of the embed's image asset"
  @type cover_image :: String.t()

  @typedoc "Application's description"
  @type description :: String.t()

  @typedoc "Id of the application's icon"
  @type icon :: String.t()

  @typedoc "Name of the application"
  @type name :: String.t()

  @type t :: %__MODULE__{
          id: id,
          cover_image: cover_image,
          description: description,
          icon: icon,
          name: name
        }
end
