defmodule LoadBalancer.Discord.Types.Message.Activity do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/channel#message-object-message-activity-structure)"

  defstruct [
    :type,
    :party_id
  ]

  @typedoc """
  [Type of message activity](https://discord.com/developers/docs/resources/channel#message-object-message-activity-types).
  """
  @type type :: integer

  @typedoc """
  The party id from a [rich presence event](https://discord.com/developers/docs/rich-presence/how-to).
  """
  @type party_id :: String.t() | nil

  @type t :: %__MODULE__{
          type: type,
          party_id: party_id
        }
end
