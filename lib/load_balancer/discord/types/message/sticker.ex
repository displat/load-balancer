defmodule LoadBalancer.Discord.Types.Message.Sticker do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/channel#message-object-message-sticker-structure)"

  alias LoadBalancer.Discord.Types.Snowflake

  defstruct [
    :id,
    :pack_id,
    :name,
    :description,
    :tags,
    :asset,
    :preview_asset,
    :format_type
  ]

  @typedoc "id of the sticker"
  @type id :: Snowflake.t()

  @typedoc "id of the pack the sticker is from"
  @type pack_id :: Snowflake.t()

  @typedoc "name of the sticker"
  @type name :: String.t()

  @typedoc "description of the sticker"
  @type description :: String.t()

  @typedoc "a comma-separated list of tags for the sticker"
  @type tags :: String.t() | nil

  @typedoc "sticker asset hash"
  @type asset :: String.t()

  @typedoc "sticker preview asset hash"
  @type preview_asset :: String.t() | nil

  @typedoc "type of sticker format"
  @type format_type :: integer()

  @type t :: %__MODULE__{
          id: id,
          pack_id: pack_id,
          name: name,
          description: description,
          tags: tags,
          asset: asset,
          preview_asset: preview_asset,
          format_type: format_type
        }
end
