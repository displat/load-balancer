defmodule LoadBalancer.Discord.Types.VoiceState do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/voice#voice-resource)"

  alias LoadBalancer.Discord.Types.{Channel, Guild, User}
  alias LoadBalancer.Discord.Types.Guild.Member

  defstruct [
    :guild_id,
    :channel_id,
    :user_id,
    :member,
    :session_id,
    :deaf,
    :mute,
    :self_deaf,
    :self_mute,
    :self_stream,
    :self_video,
    :suppress
  ]

  @typedoc "the guild id this voice state is for"
  @type guild_id :: Guild.id() | nil

  @typedoc "the channel id this user is connected to"
  @type channel_id :: Channel.id() | nil

  @typedoc "the user id this voice state is for"
  @type user_id :: User.id()

  @typedoc "the guild member this voice state is for"
  @type member :: Member.t() | nil

  @typedoc "the session id for this voice state"
  @type session_id :: String.t()

  @typedoc "whether this user is deafened by the server"
  @type deaf :: boolean

  @typedoc "whether this user is muted by the server"
  @type mute :: boolean

  @typedoc "whether this user is locally deafened"
  @type self_deaf :: boolean

  @typedoc "whether this user is locally muted"
  @type self_mute :: boolean

  @typedoc "whether this user is streaming using \"Go Live\""
  @type self_stream :: boolean

  @typedoc "whether this user's camera is enabled"
  @type self_video :: boolean

  @typedoc "whether this user is muted by the current user"
  @type suppress :: boolean

  @type t :: %__MODULE__{
          guild_id: guild_id,
          channel_id: channel_id,
          user_id: user_id,
          member: member,
          session_id: session_id,
          deaf: deaf,
          mute: mute,
          self_deaf: self_deaf,
          self_mute: self_mute,
          self_stream: self_stream,
          self_video: self_video,
          suppress: suppress
        }
end
