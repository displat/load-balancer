defmodule LoadBalancer.Discord.Types.Team do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/teams#data-models-team-object)"

  alias LoadBalancer.Discord.Types.{Snowflake, TeamMember}

  defstruct [
    :icon,
    :id,
    :members,
    :owner_user_id
  ]

  @typedoc "a hash of the image of the team's icon"
  @type icon :: String.t() | nil

  @typedoc "the unique id of the team"
  @type id :: Snowflake.t()

  @typedoc "the members of the team"
  @type members :: [TeamMember.t()]

  @typedoc "the user id of the current team owner"
  @type owner_user_id :: Snowflake.t()

  @type t :: %__MODULE__{
          icon: icon,
          id: id,
          members: members,
          owner_user_id: owner_user_id
        }
end
