defmodule LoadBalancer.Discord.Types.Guild.AuditLog do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/audit-log)"

  alias LoadBalancer.Discord.Types.Guild.{AuditLogEntry, Integration}
  alias LoadBalancer.Discord.Types.{User, Webhook}

  defstruct [:entries, :users, :webhooks, :integrations]

  @typedoc "Entries of this guild's audit log"
  @type entries :: [AuditLogEntry.t()]

  @typedoc "Users found in the audit log"
  @type users :: [User.t()]

  @typedoc "Webhooks found in the audit log"
  @type webhooks :: [Webhook.t()]

  @typedoc "Partial integration objects found in the audit log"
  @type integrations :: [Integration.t()]

  @type t :: %__MODULE__{
          entries: entries(),
          users: users(),
          webhooks: webhooks(),
          integrations: integrations()
        }
end
