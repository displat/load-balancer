defmodule LoadBalancer.Discord.Types.Guild.Role.Tags do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/permissions#role-object-role-tags-structure)"

  alias LoadBalancer.Discord.Types.Guild.Integration
  alias LoadBalancer.Discord.Types.Snowflake

  defstruct [
    :bot_id,
    :integration_id,
    :premium_subscriber
  ]

  @typedoc "the id of the bot this role belongs to"
  @type bot_id :: Snowflake.t() | nil

  @typedoc "the id of the integration this role belongs to"
  @type integration_id :: Integration.id() | nil

  @typedoc "whether this is the guild's premium subscriber role"
  @type premium_subscriber :: boolean() | nil

  @type t :: %__MODULE__{
          bot_id: bot_id,
          integration_id: integration_id,
          premium_subscriber: premium_subscriber
        }
end
