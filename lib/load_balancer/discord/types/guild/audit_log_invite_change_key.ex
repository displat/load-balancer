defmodule LoadBalancer.Discord.Types.Guild.AuditLogInviteChangeKey do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/audit-log#audit-log-change-object-audit-log-change-key)"

  alias LoadBalancer.Discord.Types.{Channel, Snowflake, User}

  defstruct [
    :id,
    :type,
    :code,
    :channel_id,
    :inviter_id,
    :max_uses,
    :uses,
    :max_age,
    :temporary
  ]

  @typedoc "the id of the changed entity - sometimes used in conjunction with other keys"
  @type id :: Snowflake.t()

  @typedoc "type of entity created"
  @type type :: Channel.type() | String.t()

  @typedoc "invite code changed"
  @type code :: String.t()

  @typedoc "channel for invite code changed"
  @type channel_id :: Channel.id()

  @typedoc "person who created invite code changed"
  @type inviter_id :: User.id()

  @typedoc "change to max number of times invite code can be used"
  @type max_uses :: integer()

  @typedoc "number of times invite code used changed"
  @type uses :: integer()

  @typedoc "how long invite code lasts changed"
  @type max_age :: integer()

  @typedoc "invite code is temporary/never expires"
  @type temporary :: boolean()

  @type t :: %__MODULE__{
          id: id(),
          type: type(),
          code: code(),
          channel_id: channel_id(),
          inviter_id: inviter_id(),
          max_uses: max_uses(),
          uses: uses(),
          max_age: max_age(),
          temporary: temporary()
        }
end
