defmodule LoadBalancer.Discord.Types.Guild.AuditLogChannelChangeKey do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/audit-log#audit-log-change-object-audit-log-change-key)"

  alias LoadBalancer.Discord.Types.{Channel, ChannelOverwrite, Snowflake}

  defstruct [
    :id,
    :type,
    :position,
    :topic,
    :bitrate,
    :permission_overwrites,
    :nsfw,
    :application_id,
    :rate_limit_per_user
  ]

  @typedoc "the id of the changed entity - sometimes used in conjunction with other keys"
  @type id :: Snowflake.t()

  @typedoc "type of entity created"
  @type type :: Channel.type() | String.t()

  @typedoc "text or voice channel position changed"
  @type position :: integer()

  @typedoc "text channel topic changed"
  @type topic :: String.t()

  @typedoc "voice channel bitrate changed"
  @type bitrate :: integer()

  @typedoc "permissions on a channel changed"
  @type permission_overwrites :: [ChannelOverwrite.t()]

  @typedoc "channel nsfw restriction changed"
  @type nsfw :: boolean()

  # TODO: unsure if this is a Message.Application.id() or not.
  @typedoc "application id of the added or removed webhook or bot"
  @type application_id :: Snowflake.t()

  @typedoc "amount of seconds a user has to wait before sending another message changed"
  @type rate_limit_per_user :: integer()

  @type t :: %__MODULE__{
          id: id(),
          type: type(),
          position: position(),
          topic: topic(),
          bitrate: bitrate(),
          permission_overwrites: permission_overwrites(),
          nsfw: nsfw(),
          application_id: application_id(),
          rate_limit_per_user: rate_limit_per_user()
        }
end
