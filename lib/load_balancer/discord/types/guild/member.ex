defmodule LoadBalancer.Discord.Types.Guild.Member do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/guild#guild-member-object-guild-member-structure)"

  alias LoadBalancer.Discord.Types.Guild.Role
  alias LoadBalancer.Discord.Types.User

  defstruct [
    :user,
    :nick,
    :roles,
    :joined_at,
    :premium_since,
    :deaf,
    :mute,
    :pending
  ]

  @typedoc "the user this guild member represents"
  @type user :: User.t() | nil

  @typedoc "this users guild nickname"
  @type nick :: String.t() | nil

  @typedoc "array of role object ids"
  @type roles :: [Role.id()]

  @typedoc "ISO8601 timestamp when the user joined the guild"
  @type joined_at :: String.t()

  @typedoc "ISO8601 timestamp when the user started boosting the guild"
  @type premium_since :: String.t() | nil

  @typedoc "whether the user is deafened in voice channels"
  @type deaf :: boolean()

  @typedoc "whether the user is muted in voice channels"
  @type mute :: boolean()

  @typedoc "whether the user has not yet passed the guild's Membership Screening requirements"
  @type pending :: boolean() | nil

  @type t :: %__MODULE__{
          user: user,
          nick: nick,
          roles: roles,
          joined_at: joined_at,
          premium_since: premium_since,
          deaf: deaf,
          mute: mute,
          pending: pending
        }
end
