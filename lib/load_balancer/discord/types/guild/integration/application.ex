defmodule LoadBalancer.Discord.Types.Guild.Integration.Application do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/guild#integration-application-object)"

  alias LoadBalancer.Discord.Types.{Snowflake, User}

  defstruct [
    :id,
    :name,
    :icon,
    :description,
    :summary,
    :bot
  ]

  @typedoc "the id of the app"
  @type id :: Snowflake.t()

  @typedoc "the name of the app"
  @type name :: String.t()

  @typedoc "the icon hash of the app"
  @type icon :: String.t() | nil

  @typedoc "the description of the app"
  @type description :: String.t()

  @typedoc "the description of the app"
  @type summary :: String.t()

  @typedoc "the bot associated with this application"
  @type bot :: User.t()

  @type t :: %__MODULE__{
          id: id,
          name: name,
          icon: icon,
          description: description,
          summary: summary,
          bot: bot
        }
end
