defmodule LoadBalancer.Discord.Types.Guild.AuditLogEntry do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/audit-log#audit-log-entry-object)"

  alias LoadBalancer.Discord.Types.{Snowflake, User}

  defstruct [
    :action_type,
    :changes,
    :id,
    :options,
    :reason,
    :target_id,
    :user_id
  ]

  @typedoc """
  Type of action that occurred.
  See [the documentation](https://discord.com/developers/docs/resources/audit-log#audit-log-entry-object-audit-log-events)
  """
  @type action_type :: pos_integer()

  @typep change_value :: String.t() | Snowflake.t() | integer() | [map()] | boolean()

  @typedoc "The new and old values of the audit log change key along with its name as a String"
  @type audit_log_change :: %{
          optional(:old_value) => change_value(),
          optional(:new_value) => change_value(),
          :key => String.t()
        }

  @typedoc """
  Changes made to the affected entity.
  See [the documentation](https://discord.com/developers/docs/resources/audit-log#audit-log-change-object-audit-log-change-key)
  """
  @type changes :: [audit_log_change()] | nil

  @typedoc "The ID of this entry"
  @type id :: Snowflake.t()

  @typedoc """
  Additional information for certain action types.
  See [the documentation](https://discord.com/developers/docs/resources/audit-log#audit-log-entry-object-optional-audit-entry-info)
  """
  @type options :: map() | nil

  @typedoc """
  The reason for this change, if applicable. Between 0 and 512 characters.
  """
  @type reason :: String.t() | nil

  @typedoc "The ID of the affected entity"
  @type target_id :: String.t() | nil

  @typedoc "The user who made the changes"
  @type user_id :: User.id()

  @type t :: %__MODULE__{
          action_type: action_type,
          changes: changes,
          id: id,
          options: options,
          reason: reason,
          target_id: target_id,
          user_id: user_id
        }
end
