defmodule LoadBalancer.Discord.Types.User.Connection do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/user#connection-object)"

  alias LoadBalancer.Discord.Types.Guild.Integration

  defstruct [
    :id,
    :name,
    :type,
    :revoked,
    :integrations,
    :verified,
    :friend_sync,
    :show_activity,
    :visibility
  ]

  @typedoc "id of the connection account"
  @type id :: String.t()

  @typedoc "the username of the connection account"
  @type name :: String.t()

  @typedoc "the service of the connection (twitch, youtube)"
  @type type :: String.t()

  @typedoc "whether the connection is revoked"
  @type revoked :: boolean | nil

  @typedoc "an array of partial server integrations"
  @type integrations :: [Integration.t()] | nil

  @typedoc "whether the connection is verified"
  @type verified :: boolean

  @typedoc "whether friend sync is enabled for this connection"
  @type friend_sync :: boolean

  @typedoc "whether activities related to this connection will be shown in presence updates"
  @type show_activity :: boolean

  @typedoc "visibility of this connection"
  @type visibility :: visibility_none | visibility_everyone

  @typedoc "invisible to everyone except the user themselves"
  @type visibility_none :: 0

  @typedoc "visible to everyone"
  @type visibility_everyone :: 1

  @type t :: %__MODULE__{
          id: id,
          name: name,
          type: type,
          revoked: revoked,
          integrations: integrations,
          verified: verified,
          friend_sync: friend_sync,
          show_activity: show_activity,
          visibility: visibility
        }
end
