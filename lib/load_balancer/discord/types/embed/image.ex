defmodule LoadBalancer.Discord.Types.Embed.Image do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/channel#embed-object-embed-image-structure)"

  defstruct [
    :url,
    :proxy_url,
    :height,
    :width
  ]

  @typedoc "Image text"
  @type url :: String.t() | nil

  @typedoc "URL of image icon"
  @type proxy_url :: String.t() | nil

  @typedoc "Height of the image"
  @type height :: integer | nil

  @typedoc "Width of the image"
  @type width :: integer | nil

  @type t :: %__MODULE__{
          url: url,
          proxy_url: proxy_url,
          height: height,
          width: width
        }
end
