defmodule LoadBalancer.Discord.Types.Embed.Field do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/channel#embed-object-embed-field-structure)"

  defstruct [
    :name,
    :value,
    :inline
  ]

  @typedoc "Name of the field"
  @type name :: String.t()

  @typedoc "Value of the field"
  @type value :: String.t()

  @typedoc "Whether the field should display as inline"
  @type inline :: boolean | nil

  @type t :: %__MODULE__{
          name: name,
          value: value,
          inline: inline
        }
end
