defmodule LoadBalancer.Discord.Types.ChannelOverwrite do
  @moduledoc "See https://discord.com/developers/docs/resources/channel#overwrite-object"

  alias LoadBalancer.Discord.Types.Guild.Role
  alias LoadBalancer.Discord.Types.User

  defstruct [
    :id,
    :type,
    :allow,
    :deny
  ]

  @typedoc "role or user id"
  @type id :: Role.id() | User.id()

  @typedoc "either 0 (role) or 1 (member)"
  @type type :: type_role | type_member
  @type type_role :: 0
  @type type_member :: 1

  @typedoc "Permission bit set. See https://discord.com/developers/docs/topics/permissions#permissions"
  @type permission_bit_set :: String.t()

  @type allow :: permission_bit_set()

  @type deny :: permission_bit_set()

  @type t :: %__MODULE__{
          id: id(),
          type: type(),
          allow: allow(),
          deny: deny()
        }
end
