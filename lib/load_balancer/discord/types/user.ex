defmodule LoadBalancer.Discord.Types.User do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/resources/user)"

  alias LoadBalancer.Discord.Types.Snowflake

  defstruct [
    :id,
    :username,
    :discriminator,
    :avatar,
    :bot,
    :mfa_enabled,
    :verified,
    :email,
    :public_flags,
    :system,
    :locale,
    :flags,
    :premium_type
  ]

  @typedoc "The user's id"
  @type id :: Snowflake.t()

  @typedoc "The user's username"
  @type username :: String.t()

  @typedoc "The user's 4--digit discord-tag"
  @type discriminator :: String.t()

  @typedoc "User's avatar hash"
  @type avatar :: String.t() | nil

  @typedoc "Whether the user is a bot"
  @type bot :: boolean | nil

  @typedoc "Whether the user has two factor enabled"
  @type mfa_enabled :: boolean | nil

  @typedoc "Whether the email on the account has been verified"
  @type verified :: boolean | nil

  @typedoc "The user's email"
  @type email :: String.t() | nil

  @typedoc "The user's public flags"
  @type public_flags :: integer()

  @typedoc "whether the user is an Official Discord System user (part of the urgent message system)"
  @type system :: boolean | nil

  @typedoc "the user's chosen language option"
  @type locale :: String.t() | nil

  @typedoc "the flags on a user's account"
  @type flags :: integer | nil

  @typedoc "the type of Nitro subscription on a user's account"
  @type premium_type :: integer | nil

  @type t :: %__MODULE__{
          id: id,
          username: username,
          discriminator: discriminator,
          avatar: avatar,
          bot: bot,
          mfa_enabled: mfa_enabled,
          verified: verified,
          email: email,
          public_flags: public_flags
        }
end
