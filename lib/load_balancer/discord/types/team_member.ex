defmodule LoadBalancer.Discord.Types.TeamMember do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/teams#data-models-team-members-object)"

  alias LoadBalancer.Discord.Types.{Team, User}

  defstruct [
    :membership_state,
    :permissions,
    :team_id,
    :user
  ]

  @typedoc "the user's membership state on the team"
  @type membership_state :: integer()

  @typedoc "will always be [\"*\"]"
  @type permissions :: [String.t()]

  @typedoc "the id of the parent team of which they are a member"
  @type team_id :: Team.id()

  @typedoc "the avatar, discriminator, id, and username of the user (a *partial* user object)"
  @type user :: User.t()

  @type t :: %__MODULE__{
          membership_state: membership_state,
          permissions: permissions,
          team_id: team_id,
          user: user
        }
end
