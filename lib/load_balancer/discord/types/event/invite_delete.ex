defmodule LoadBalancer.Discord.Types.Event.InviteDelete do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/gateway#invite-delete)"

  alias LoadBalancer.Discord.Types.{Channel, Guild}

  defstruct [
    :channel_id,
    :guild_id,
    :code
  ]

  @typedoc """
  Channel id of the channel this invite is for.
  """
  @type channel_id :: Channel.id()

  @typedoc """
  Guild id of the guild this invite is for.
  """
  @type guild_id :: Guild.id() | nil

  @typedoc """
  The unique invite code.
  """
  @type code :: String.t()

  @type t :: %__MODULE__{
          channel_id: channel_id,
          guild_id: guild_id,
          code: code
        }
end
