defmodule LoadBalancer.Discord.Types.Event.GuildMemberUpdate do
  @moduledoc "See [the documentaiton](https://discord.com/developers/docs/topics/gateway#guild-member-update)"

  alias LoadBalancer.Discord.Types.{Guild, User}

  defstruct [
    :guild_id,
    :roles,
    :user,
    :nick,
    :joined_at,
    :premium_since
  ]

  @typedoc "the id of the guild"
  @type guild_id :: Guild.id()

  @typedoc "user role ids"
  @type roles :: [User.id()]

  @typedoc "the user"
  @type user :: User.t()

  @typedoc "nickname of the user in the guild"
  @type nick :: String.t() | nil

  @typedoc "ISO8601 timestamp when the user joined the guild"
  @type joined_at :: String.t()

  @typedoc "ISO8601 timestamp when the user starting boosting the guild"
  @type premium_since :: String.t() | nil

  @type t :: %__MODULE__{
          guild_id: guild_id,
          roles: roles,
          user: user,
          nick: nick,
          joined_at: joined_at,
          premium_since: premium_since
        }
end
