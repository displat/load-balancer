defmodule LoadBalancer.Discord.Types.Event.MessageDeleteBulk do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/gateway#message-delete-bulk)"

  alias LoadBalancer.Discord.Types.Snowflake

  defstruct [
    :channel_id,
    :guild_id,
    :ids
  ]

  @typedoc "Channel id of the deleted message"
  @type channel_id :: Snowflake.t()

  @typedoc """
  Guild id of the deleted message

  `nil` if a non-guild message was deleted.
  """
  @type guild_id :: Snowflake.t() | nil

  @typedoc "Ids of the deleted messages"
  @type ids :: [Snowflake.t(), ...]

  @type t :: %__MODULE__{
          channel_id: channel_id,
          guild_id: guild_id,
          ids: ids
        }
end
