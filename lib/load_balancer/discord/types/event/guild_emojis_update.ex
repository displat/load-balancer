defmodule LoadBalancer.Discord.Types.GuildEmojisUpdate do
  @moduledoc "See [the documentation] (https://discord.com/developers/docs/topics/gateway#guild-emojis-update-guild-emojis-update-event-fields)"

  alias LoadBalancer.Discord.Types.{Emoji, Guild}

  defstruct [
    :guild_id,
    :emojis
  ]

  @typedoc "id of the guild"
  @type guild_id :: Guild.id()

  @typedoc "array of emojis"
  @type emojis :: [Emoji.t()]

  @type t :: %__MODULE__{
          guild_id: guild_id,
          emojis: emojis
        }
end
