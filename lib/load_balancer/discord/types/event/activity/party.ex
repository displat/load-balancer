defmodule LoadBalancer.Discord.Types.Event.Activity.Party do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/gateway#activity-object-activity-party)"

  defstruct [
    :id,
    :size
  ]

  @typedoc "the id of the party"
  @type id :: String.t() | nil

  @typedoc "array of two integers (current_size, max_size) used to show the party's current and maximum size"
  @type size :: [integer()] | nil

  @type t :: %__MODULE__{
          id: id,
          size: size
        }
end
