defmodule LoadBalancer.Discord.Types.Event.Activity.Secrets do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/gateway#activity-object-activity-secrets)"

  defstruct [
    :join,
    :spectate,
    :match
  ]

  @typedoc "the secret for joining a party"
  @type join :: String.t() | nil

  @typedoc "the secret for spectating a game"
  @type spectate :: String.t() | nil

  @typedoc "the secret for a specific instanced match"
  @type match :: String.t() | nil

  @type t :: %__MODULE__{
          join: join,
          spectate: spectate,
          match: match
        }
end
