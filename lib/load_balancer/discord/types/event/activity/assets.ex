defmodule LoadBalancer.Discord.Types.Event.Activity.Assets do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/gateway#activity-object-activity-assets)"

  defstruct [
    :large_image,
    :large_text,
    :small_image,
    :small_text
  ]

  @typedoc "the id for a large asset of the activity, usually a snowflake"
  @type large_image :: String.t() | nil

  @typedoc "text displayed when hovering over the large image of the activity"
  @type large_text :: String.t() | nil

  @typedoc "the id for a small asset of the activity, usually a snowflake"
  @type small_image :: String.t() | nil

  @typedoc "text displayed when hovering over the small image of the activity"
  @type small_text :: String.t() | nil

  @type t :: %__MODULE__{
          large_image: large_image,
          large_text: large_text,
          small_image: small_image,
          small_text: small_text
        }
end
