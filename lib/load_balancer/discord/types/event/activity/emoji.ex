defmodule LoadBalancer.Discord.Types.Event.Activity.Emoji do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/gateway#activity-object-activity-emoji)"

  alias LoadBalancer.Discord.Types.Snowflake

  defstruct [
    :name,
    :id,
    :animated
  ]

  @typedoc "the name of the emoji"
  @type name :: String.t()

  @typedoc "the id of the emoji"
  @type id :: Snowflake.t() | nil

  @typedoc "whether this emoji is animated"
  @type animated :: boolean() | nil

  @type t :: %__MODULE__{
          name: name,
          id: id,
          animated: animated
        }
end
