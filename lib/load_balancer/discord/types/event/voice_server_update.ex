defmodule LoadBalancer.Discord.Types.Event.VoiceServerUpdate do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/gateway#voice-server-update-voice-server-update-event-fields)"

  alias LoadBalancer.Discord.Types.Guild

  defstruct [
    :token,
    :guild_id,
    :endpoint
  ]

  @typedoc "voice connection token"
  @type token :: String.t()

  @typedoc "the guild this voice server update is for"
  @type guild_id :: Guild.id()

  @typedoc "the voice server host"
  @type endpoint :: String.t()

  @type t :: %__MODULE__{
          token: token,
          guild_id: guild_id,
          endpoint: endpoint
        }
end
