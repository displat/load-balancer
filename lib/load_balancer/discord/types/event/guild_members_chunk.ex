defmodule LoadBalancer.Discord.Types.Event.GuildMembersChunk do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/gateway#guild-members-chunk)"

  alias LoadBalancer.Discord.Types.Event.PresenceUpdate
  alias LoadBalancer.Discord.Types.{Guild, Snowflake}
  alias LoadBalancer.Discord.Types.Guild.Member

  defstruct [
    :guild_id,
    :members,
    :chunk_index,
    :chunk_count,
    :not_found,
    :presences,
    :nonce
  ]

  @typedoc "the id of the guild"
  @type guild_id :: Guild.id()

  @typedoc "set of guild members"
  @type members :: [Member.t()]

  @typedoc "the chunk index in the expected chunks for this response (0 <= chunk_index < chunk_count)"
  @type chunk_index :: integer()

  @typedoc "the total number of expected chunks for this response"
  @type chunk_count :: integer()

  @typedoc "if passing an invalid id to REQUEST_GUILD_MEMBERS, it will be returned here"
  @type not_found :: [Snowflake.t()] | nil

  @typedoc "if passing true to REQUEST_GUILD_MEMBERS, presences of the returned members will be here"
  @type presences :: [PresenceUpdate.t()] | nil

  @typedoc "the nonce used in the Guild Members Request"
  @type nonce :: String.t() | nil

  @type t :: %__MODULE__{
          guild_id: guild_id,
          members: members,
          chunk_index: chunk_index,
          chunk_count: chunk_count,
          not_found: not_found,
          presences: presences,
          nonce: nonce
        }
end
