defmodule LoadBalancer.Discord.Types.Event.Activity do
  @moduledoc "See [the documentation](https://discord.com/developers/docs/topics/gateway#activity-object)"

  alias LoadBalancer.Discord.Types.Event.Activity.{Assets, Emoji, Party, Secrets, Timestamps}
  alias LoadBalancer.Discord.Types.Guild.Integration

  defstruct [
    :name,
    :type,
    :url,
    :created_at,
    :timestamps,
    :application_id,
    :details,
    :state,
    :emoji,
    :party,
    :assets,
    :secrets,
    :instance,
    :flags
  ]

  @typedoc "the activity's name"
  @type name :: String.t()

  @typedoc "activity type"
  @type type :: integer()

  @typedoc "stream url, is validated when type is 1"
  @type url :: String.t() | nil

  @typedoc "unix timestamp of when the activity was added to the user's session"
  @type created_at :: integer()

  @typedoc "unix timestamps for start and/or end of the game"
  @type timestamps :: Timestamps.t() | nil

  @typedoc "application id for the game"
  @type application_id :: Integration.id() | nil

  @typedoc "what the player is currently doing"
  @type details :: String.t() | nil

  @typedoc "the user's current party status"
  @type state :: String.t() | nil

  @typedoc "the emoji used for a custom status"
  @type emoji :: Emoji.t() | nil

  @typedoc "information for the current party of the player"
  @type party :: Party.t() | nil

  @typedoc "images for the presence and their hover texts"
  @type assets :: Assets.t() | nil

  @typedoc "secrets for Rich Presence joining and spectating"
  @type secrets :: Secrets.t() | nil

  @typedoc "whether or not the activity is an instanced game session"
  @type instance :: boolean() | nil

  @typedoc "activity flags ORd together, describes what the payload includes"
  @type flags :: integer() | nil

  @type t :: %__MODULE__{
          name: name,
          type: type,
          url: url,
          created_at: created_at,
          timestamps: timestamps,
          application_id: application_id,
          details: details,
          state: state,
          emoji: emoji,
          party: party,
          assets: assets,
          secrets: secrets,
          instance: instance,
          flags: flags
        }
end
