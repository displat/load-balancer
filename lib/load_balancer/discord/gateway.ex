defmodule LoadBalancer.Discord.Gateway do
  @moduledoc """
  Gateway manager. Isolates operations from connection events
  """

  import Logger

  alias LoadBalancer.Discord.ConnManager

  @doc """
  Handles the heartbeat loop
  """
  def heartbeat(%{hb_pending: true} = state) do
    warning("Heartbeat missed - reconnecting")

    ConnManager.cycle_self(state[:shard])
  end

  def heartbeat(%{hb_interval: interval} = state) do
    seq = state[:seq]

    debug("Sending heartbeat")

    send(self(), {:send, %{"op" => 1, "d" => seq}})

    Process.send_after(self(), :heartbeat, interval)

    Map.put(state, :hb_pending, true)
  end

  @doc """
  Creates and sends a Identify packet
  """
  def identify(state) do
    debug("Sending identify")

    id = %{
      "op" => 2,
      "d" => %{
        "token" => state[:token],
        "intents" => state[:intents] || 0,
        "properties" => %{
          "$os" => state[:os],
          "$browser" => "displat",
          "$device" => "displat"
        }
      }
    }

    id =
      if Map.has_key?(state, :shard) do
        put_in(id, ["d", "shard"], [state[:shard], state[:shards]])
      else
        id
      end

    send(self(), {:send, id})

    state
  end

  @doc """
  Creates and sends a Resume packet
  """
  def resume(session_id, state) do
    debug("Sending resume")

    seq = ConnManager.get_seq(state[:shard])

    resume = %{
      "op" => 6,
      "d" => %{
        "token" => state[:token],
        "session_id" => session_id,
        "seq" => seq
      }
    }

    send(self(), {:send, resume})

    state
  end

  ####
  # Packet handlers
  ####

  @doc """
  Handles the various op codes
  """
  # Op Code 0 - Dispatch
  #
  # Response: update sequence value and pass dispatch to cache (and others?)
  def handle_packet(%{"op" => 0, "d" => d, "s" => seq, "t" => type}, state) do
    send(self(), {:dispatch, type, d})

    ConnManager.set_seq(state[:shard], seq)

    Map.put(state, :seq, seq)
  end

  # Op Code 1 - Heartbeat
  #
  # Respose: send an Op 11 (Heartbeat Ack) w/ last processed sequence value
  def handle_packet(%{"op" => 1}, state) do
    debug("Acknowledging heartbeat")

    ack = %{"op" => 11, "d" => state[:seq]}

    send(self(), {:send, ack})

    state
  end

  # Op Code 7 - Reconnect
  #
  # Response: reconnect and resume. Reconnects are initiated with a fresh Connection process, which will
  # fetch Resume state on Hello
  def handle_packet(%{"op" => 7}, state) do
    warning("Reconnect received, restarting connection")

    ConnManager.cycle_self(state[:shard])
  end

  # Op Code 9 - Invalid Session
  #
  # Response: reconnect and resume, unless `d` is `false`.
  def handle_packet(%{"op" => 9, "d" => resume}, state) do
    warning("Invalid Session received, restarting connection - Resuming: #{resume}")

    ConnManager.cycle_self(state[:shard], resume)
  end

  # Op Code 10 - Hello
  #
  # Response: start Heartbeat loop, and send Identify or Resume, depending on state of previous connection, if any
  def handle_packet(%{"op" => 10, "d" => %{"heartbeat_interval" => interval}}, state) do
    state = Map.put(state, :hb_interval, interval)

    if session_id = ConnManager.get_resume(state[:shard]) do
      send(self(), {:resume, session_id})
    else
      send(self(), :identify)
    end

    send(self(), :heartbeat)

    state
  end

  # Op Code 11 - Heartbeat Ack
  #
  # Response: acknowledge ack internally; lack of this is grounds for immediate disconnect
  def handle_packet(%{"op" => 11}, state) do
    debug("Received heartbeat ack")

    Map.put(state, :hb_pending, false)
  end

  # Fall through catch - log all unknown opcodes - may clue people into the need to update
  def handle_packet(packet, state) do
    warning("Received unknown opcode #{packet[:op]} - #{inspect(packet)}")

    state
  end

  ####
  # Dispatch (Op 0)
  ####

  def handle_dispatch("READY", %{"session_id" => session} = _d, state) do
    info("Received Ready")

    # TODO: add UserSelf data to cache

    ConnManager.set_session(state[:shard], session)

    Map.put(state, :session_id, session)
  end
end
