defmodule LoadBalancer.Discord.WS do
  @moduledoc """
  A WS connection wrapping a Gateway implementation.
  """

  use GenServer

  import Logger

  alias LoadBalancer.Discord.ConnManager
  alias LoadBalancer.Discord.Gateway

  def start_link(%{url: url, token: token, shard: shard, shards: shards, intents: intents}) do
    GenServer.start_link(
      __MODULE__,
      {url, %{token: token, shard: shard, shards: shards, intents: intents}}
    )
  end

  def start_link(%{url: url, token: token, intents: intents}) do
    GenServer.start_link(__MODULE__, {url, %{token: token, intents: intents}})
  end

  @impl true
  def init({url, state}) do
    info("Starting connection")

    # Get the OS name as required by Discord
    {_, os_name} = :os.type()
    os = Atom.to_string(os_name)

    uri = URI.parse(url)

    # Start and complete the WS connection
    {:ok, _} = :application.ensure_all_started(:gun)
    {:ok, conn_pid} = :gun.open(uri.host |> String.to_charlist(), 443, %{protocols: [:http]})
    {:ok, _protocol} = :gun.await_up(conn_pid)
    stream_ref = :gun.ws_upgrade(conn_pid, '/?v=8&encoding=json')

    info("Connection established")

    # Add to state.
    state =
      state
      |> Map.put(:os, os)
      |> Map.put(:stream_ref, stream_ref)
      |> Map.put(:client, conn_pid)

    {:ok, state}
  end

  ####
  # gun messages
  ####

  def handle_info({:gun_upgrade, _gun_pid, _stream_ref, ["websocket"], _headers}, state) do
    {:noreply, state}
  end

  def handle_info({:gun_down, gun_pid, _http_ws, :closed, [], []}, %{gun_pid: gun_pid} = state) do
    {:noreply, state}
  end

  def handle_info(
        {:gun_down, _alt_gun_pid, _http_ws, :closed, [], []},
        %{gun_pid: _gun_pid} = state
      ) do
    {:noreply, state}
  end

  def handle_info(
        {:gun_ws, gun_pid, stream_ref, {:close, code, msg}},
        %{stream_ref: stream_ref, gun_pid: gun_pid} = state
      ) do
    error("Received close code #{code} - #{msg}")

    ConnManager.cycle_self(state[:shard])
  end

  def handle_info({:gun_up, gun_pid, _http_ws}, %{gun_pid: gun_pid} = state) do
    {:noreply, state}
  end

  def handle_info({:gun_up, _alt_gun_pid, _http_ws}, %{gun_pid: _gun_pid} = state) do
    {:noreply, state}
  end

  def handle_info(
        {:gun_ws, client, stream_ref, {:text, frame}},
        %{stream_ref: stream_ref, client: client} = state
      ) do
    debug("Recieving packet")

    packet = Poison.decode!(frame)

    debug("Received Packet - #{inspect(packet)}")

    state = Gateway.handle_packet(packet, state)

    {:noreply, state}
  end

  ####
  # Internal events
  ####

  def handle_info({:send, frame}, state) do
    send_frame(frame, state)

    {:noreply, state}
  end

  def handle_info(:heartbeat, state) do
    state = Gateway.heartbeat(state)

    {:noreply, state}
  end

  def handle_info(:identify, state) do
    state = Gateway.identify(state)

    {:noreply, state}
  end

  def handle_info({:resume, session_id}, state) do
    state = Gateway.resume(session_id, state)

    {:noreply, state}
  end

  def handle_info({:dispatch, type, d}, state) do
    state = Gateway.handle_dispatch(type, d, state)

    {:noreply, state}
  end

  ####
  # Unhandled msg logging
  ####

  @impl true
  def handle_info(msg, state) do
    debug("Unexpected message: #{inspect(msg)} - state: #{inspect(state)}")

    {:noreply, state}
  end

  @impl true
  def handle_cast(msg, state) do
    debug("Unexpected cast: #{inspect(msg)} - state: #{inspect(state)}")

    {:noreply, state}
  end

  @impl true
  def handle_call(msg, caller, state) do
    debug("Unexpected call from #{inspect(caller)}: #{inspect(msg)} - state: #{inspect(state)}")

    {:reply, :ok, state}
  end

  ####
  # Utility functions
  ####

  defp send_frame(term, state) do
    :ok = :gun.ws_send(state[:client], {:text, Poison.encode!(term)})
  end
end
