defmodule LoadBalancer.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    conn_opts = %{
      token: Application.fetch_env!(:displat_lb, :token),
      shards: Application.get_env(:displat_lb, :shards),
      intents: Application.get_env(:displat_lb, :intents, 0)
    }

    children = [
      {Finch,
       name: LoadBalancer.Http,
       pools: %{
         :default => [size: 4],
         "https://discord.com/api" => [protocol: :http2]
       }},
      {DynamicSupervisor, strategy: :one_for_one, name: LoadBalancer.Discord.DynamicSupervisor},
      {LoadBalancer.Discord.ConnManager, conn_opts}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: LoadBalancer.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
